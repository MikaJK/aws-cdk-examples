package com.pikecape;

import software.amazon.awscdk.core.App;

public class AwsCdkLambdaApp {
    public static void main(final String[] args) {
        App app = new App();

        new AwsCdkLambdaStack(app, "AwsCdkLambdaStack");

        app.synth();
    }
}
