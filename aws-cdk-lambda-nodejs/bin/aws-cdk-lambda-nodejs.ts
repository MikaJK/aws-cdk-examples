#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { AwsCdkLambdaNodejsStack } from '../lib/aws-cdk-lambda-nodejs-stack';

const app = new cdk.App();
new AwsCdkLambdaNodejsStack(app, 'AwsCdkLambdaNodejsStack');
