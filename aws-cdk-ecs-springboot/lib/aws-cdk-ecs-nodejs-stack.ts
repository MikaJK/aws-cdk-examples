import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as ecs from '@aws-cdk/aws-ecs';
import * as ecs_pattrns from '@aws-cdk/aws-ecs-patterns';
import * as path from 'path';

export class AwsCdkEcsNodejsStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'MyVpc', {
      maxAzs: 2
    });

    const cluster = new ecs.Cluster(this, 'MyCluster', {
      vpc
    });

    new ecs_pattrns.ApplicationLoadBalancedFargateService(this, 'MyFargateService', {
      cluster,
      taskImageOptions: {
        image: ecs.ContainerImage.fromAsset('local_image')
      }
    });
  }
}
