#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { AwsCdkEcsNodejsStack } from '../lib/aws-cdk-ecs-nodejs-stack';

const app = new cdk.App();
new AwsCdkEcsNodejsStack(app, 'AwsCdkEcsNodejsStack');
