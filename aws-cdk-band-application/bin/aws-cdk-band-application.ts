#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { AwsCdkBandApplicationStack } from '../lib/aws-cdk-band-application-stack';

const app = new cdk.App();
new AwsCdkBandApplicationStack(app, 'AwsCdkBandApplicationStack');
