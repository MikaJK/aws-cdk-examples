'use strict';
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

/**
 * Update band.
 */
exports.handler = async (event, context) => {

    let responseBody = "";
    let statusCode = 0;

    const { bandid } = event.pathParameters;

    const { band_name, genre } = JSON.parse(event.body);

    const params = {
        TableName: "bands",
        Key: {
            id: parseInt(bandid)
        },
        UpdateExpression: "set band_name = :n, genre = :g",
        ExpressionAttributeValues: {
            ":n": band_name,
            ":g": genre
        },
        ReturnValues: "ALL_NEW"
    };

    try {
        const data = await documentClient.update(params).promise();
        responseBody = JSON.stringify(data.Attributes);
        statusCode = 200;
    } catch(err) {
        responseBody = `Unable to update band information: ${err}`;
        statusCode = 400;
    }

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "application/json",
            "access-control-allow-origin": "*"
        },
        body: responseBody
    };

    return response;
};