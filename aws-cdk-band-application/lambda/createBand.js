'use strict';
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

/**
 * Create band.
 */
exports.handler = async (event, context) => {
    
    let responseBody;
    let statusCode;
    let response;

    const { band_name, genre } = JSON.parse(event.body);

    await createEntry(band_name, genre)
        .then(data => {
            statusCode = 201;
            responseBody = data;
        })
        .catch(err => {
            statusCode = 400;
            responseBody = err;
        });

    response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "application/json",
            "access-control-allow-origin": "*"
        },
        body: responseBody
    };

    return response;
};

/**
 * Create entry.
 * 
 * @param {} band_name 
 * @param {*} genre 
 */
function createEntry(band_name, genre) {
    return new Promise(async function(resolve, reject) {
        
        getNextId()
            .then( async function(id) {

                var params = {
                    TableName: "bands",
                    Key: {
                        id: id,
                    },
                    UpdateExpression: "set band_name = :n, genre = :g",
                    ExpressionAttributeValues: {
                        ":n": band_name,
                        ":g": genre
                    },
                    ReturnValues: "ALL_NEW"
                };
                
                try {
                    const data = await documentClient.update(params).promise();
                    resolve(JSON.stringify(data.Attributes));
                } catch(err) {
                    reject(`Unable to create band information: ${err}`);
                }
            })
            .catch(err => {
                reject(err);
            });
    });
};

/**
 * Get next id.
 */
function getNextId() {
    return new Promise(async function(resolve, reject) {
        
        let nextId = 0;
        const params = {
            TableName: "bands"
        };

        try {
            const bands = await documentClient.scan(params).promise();

            bands.Items.forEach(band => {
                if (band.id > nextId)
                {
                    nextId = band.id;
                }
            });

            nextId++;

            resolve(nextId);
        } catch(err) {
            reject(`Unable to define band identifier: ${err}`); 
        }
    });
};