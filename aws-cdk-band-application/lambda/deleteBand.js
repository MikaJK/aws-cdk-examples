'use strict';
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

/**
 * Delete band.
 */
exports.handler = async (event, context) => {

    let responseBody = "";
    let statusCode = 0;

    const { bandid } = event.pathParameters;

    const params = {
        TableName: "bands",
        Key: {
            id: parseInt(bandid)
        }
    };

    try {
        const data = await documentClient.delete(params).promise();
        responseBody = JSON.stringify(data);
        statusCode = 204;
    } catch(err) {
        responseBody = `Unable to delete band information: ${err}`;
        statusCode = 404;
    }

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "application/json",
            "access-control-allow-origin": "*"
        },
        body: responseBody
    };

    return response;
};