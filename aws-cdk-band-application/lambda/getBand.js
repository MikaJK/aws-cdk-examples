'use strict';
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

/**
 * Get band.
 */
exports.handler = async (event, context) => {

    let responseBody = "";
    let statusCode = 0;

    const { bandid } = event.pathParameters;

    const params = {
        TableName: "bands",
        FilterExpression: "id = :i",
        ExpressionAttributeValues:
        {
            ":i": parseInt(bandid)
        }
    };

    try {
        const data = await documentClient.scan(params).promise();
        
        if (data.Items.length > 0) {
            responseBody = JSON.stringify(data.Items[0]);
            statusCode = 200;    
        } else {
            responseBody = `Band not ${bandid} found.`;
            statusCode = 404;
        }
    } catch(err) {
        responseBody = `Band not ${bandid} found: ${err}`;
        statusCode = 500;
    }

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "application/json",
            "access-control-allow-origin": "*"
        },
        body: responseBody
    };

    return response;
};
