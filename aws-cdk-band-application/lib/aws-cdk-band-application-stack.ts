import * as cdk from '@aws-cdk/core';
import * as dynamodb from '@aws-cdk/aws-dynamodb';
import * as lambda from '@aws-cdk/aws-lambda';
import * as apigateway from '@aws-cdk/aws-apigateway';

export class AwsCdkBandApplicationStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const bandTable = new dynamodb.Table(this, 'bands', {
      partitionKey: {
        name: 'id',
        type: dynamodb.AttributeType.NUMBER
      },
      tableName: 'bands',

      // The default removal policy is RETAIN, which means that cdk destroy will not attempt to delete
      // the new table, and it will remain in your account until manually deleted. By setting the policy to 
      // DESTROY, cdk destroy will delete the table (even if it has data in it)
      removalPolicy: cdk.RemovalPolicy.RETAIN, // NOT recommended for production code
    });

    const getOneBand = new lambda.Function(this, 'getOneBandFunction', {
      code: lambda.Code.fromAsset('lambda'),
      handler: 'getBand.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: bandTable.tableName,
        PRIMARY_KEY: 'id'
      }
    });

    const getAllBands = new lambda.Function(this, 'getAllBandsFunction', {
      code: lambda.Code.fromAsset('lambda'),
      handler: 'getBands.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: bandTable.tableName,
        PRIMARY_KEY: 'id'
      }
    });

    const createBand = new lambda.Function(this, 'createBandFunction', {
      code: lambda.Code.fromAsset('lambda'),
      handler: 'createBand.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: bandTable.tableName,
        PRIMARY_KEY: 'id'
      }
    });

    const updateBand = new lambda.Function(this, 'updateBandFunction', {
      code: lambda.Code.fromAsset('lambda'),
      handler: 'updateBand.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: bandTable.tableName,
        PRIMARY_KEY: 'id'
      }
    });

    const deleteBand = new lambda.Function(this, 'deleteBandFunction', {
      code: lambda.Code.fromAsset('lambda'),
      handler: 'deleteBand.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: bandTable.tableName,
        PRIMARY_KEY: 'id'
      }
    });

    bandTable.grantReadWriteData(getOneBand);
    bandTable.grantReadWriteData(getAllBands);
    bandTable.grantReadWriteData(createBand);
    bandTable.grantReadWriteData(updateBand);
    bandTable.grantReadWriteData(deleteBand);

    const bandApi = new apigateway.RestApi(this, 'bandApi', {
      restApiName: 'Band Service'
    });

    const bands = bandApi.root.addResource('bands');

    const getAllBandsIntegration = new apigateway.LambdaIntegration(getAllBands);
    bands.addMethod('GET', getAllBandsIntegration);

    const createBandIntegration = new apigateway.LambdaIntegration(createBand);
    bands.addMethod('POST', createBandIntegration);
    
    addCorsOptions(bands);

    const singleBand = bands.addResource('{bandid}');
    const getOneBandIntegration = new apigateway.LambdaIntegration(getOneBand);
    singleBand.addMethod('GET', getOneBandIntegration);

    const updateBandIntegration = new apigateway.LambdaIntegration(updateBand);
    singleBand.addMethod('PUT', updateBandIntegration);

    const deleteBandIntegration = new apigateway.LambdaIntegration(deleteBand);
    singleBand.addMethod('DELETE', deleteBandIntegration);
    
    addCorsOptions(singleBand);
  }
}

export function addCorsOptions(apiResource: apigateway.IResource) {
  apiResource.addMethod('OPTIONS', new apigateway.MockIntegration({
    integrationResponses: [{
      statusCode: '200',
      responseParameters: {
        'method.response.header.Access-Control-Allow-Headers': "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent'",
        'method.response.header.Access-Control-Allow-Origin': "'*'",
        'method.response.header.Access-Control-Allow-Credentials': "'false'",
        'method.response.header.Access-Control-Allow-Methods': "'OPTIONS,GET,PUT,POST,DELETE'",
      },
    }],
    passthroughBehavior: apigateway.PassthroughBehavior.NEVER,
    requestTemplates: {
      "application/json": "{\"statusCode\": 200}"
    },
  }), {
    methodResponses: [{
      statusCode: '200',
      responseParameters: {
        'method.response.header.Access-Control-Allow-Headers': true,
        'method.response.header.Access-Control-Allow-Methods': true,
        'method.response.header.Access-Control-Allow-Credentials': true,
        'method.response.header.Access-Control-Allow-Origin': true,
      },  
    }]
  })
}
