#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { AwsCdkTypescriptLambdaStack } from '../lib/aws-cdk-typescript-lambda-stack';

const app = new cdk.App();
new AwsCdkTypescriptLambdaStack(app, 'AwsCdkTypescriptLambdaStack');
