import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as apigw from '@aws-cdk/aws-apigateway';

export class AwsCdkTypescriptLambdaStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const english = new lambda.Function(this, 'EnglishService', {
      runtime: lambda.Runtime.NODEJS_10_X,
      code: lambda.Code.fromAsset('lambda'),
      handler: 'hello.english'
    });

    const spanish = new lambda.Function(this, 'SpanishService', {
      runtime: lambda.Runtime.NODEJS_10_X,
      code: lambda.Code.fromAsset('lambda'),
      handler: 'hello.spanish'
    });

    const helloApi = new apigw.RestApi(this, 'Endpoint', {
      restApiName: "Hello Service"
    });

    const getHello = new apigw.LambdaIntegration(english, {
      requestTemplates: { "application/json": '{ "statusCode": "200" }' }
    });

    helloApi.root.addMethod('GET', getHello);
  }
}
