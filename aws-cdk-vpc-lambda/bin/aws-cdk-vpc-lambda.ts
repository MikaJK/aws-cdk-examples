#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { AwsCdkVpcLambdaStack } from '../lib/aws-cdk-vpc-lambda-stack';

const app = new cdk.App();
new AwsCdkVpcLambdaStack(app, 'AwsCdkVpcLambdaStack');
